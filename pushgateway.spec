%define debug_package %{nil}

Name:           pushgateway
Version:        1.9.0
Release:        1
Summary:        Prometheus pushgateway.
License:        ASL 2.0
URL:            https://github.com/prometheus/pushgateway
Source0:        https://github.com/prometheus/pushgateway/archive/v%{version}/%{name}-%{version}.tar.gz
Source1:        %{name}.service
Source2:        %{name}.default
# tar -xvf Source0
# run 'go mod vendor' in it
# tar -czvf pushgateway-vendor.tar.gz vendor
Source3:        pushgateway-vendor.tar.gz
Patch0:         add-parameters-to-solve-the-strip.patch

BuildRequires:  golang >= 1.14
BuildRequires:  promu
BuildRequires:  systemd
%{?systemd_requires}
Requires(pre):  shadow-utils

Provides:       %{name} = %{version}-%{release}

%description
The Prometheus Pushgateway exists to allow ephemeral and batch jobs to expose their metrics to Prometheus.
Since these kinds of jobs may not exist long enough to be scraped, they can instead push their metrics to a Pushgateway.
The Pushgateway then exposes these metrics to Prometheus.

%prep
%setup -q -n %{name}-%{version}
tar xvf %{SOURCE3} -C .
%patch 0 -p1

%build
export GOFLAGS="-mod=vendor"
promu build

%install
install -D -m 0755 %{name}-%{version} %{buildroot}%{_bindir}/%{name}
mkdir -vp %{buildroot}%{_sharedstatedir}/prometheus
install -D -m 644 %{SOURCE1} %{buildroot}%{_unitdir}/%{name}.service
install -D -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/default/%{name}

%pre
getent group prometheus >/dev/null || groupadd -r prometheus
getent passwd prometheus >/dev/null || \
  useradd -r -g prometheus -d %{_sharedstatedir}/prometheus -s /sbin/nologin \
          -c "Prometheus services" prometheus
exit 0

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun %{name}.service

%files
%defattr(-,root,root,-)
%{_bindir}/*
%{_unitdir}/%{name}.service
%config(noreplace) %{_sysconfdir}/default/%{name}
%dir %attr(755, prometheus, prometheus)%{_sharedstatedir}/prometheus

%changelog
* Wed Jun 26 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 1.9.0-1
- Update package to version 1.9.0
- resources: naive rendering for native histograms
- diskmetricsstore: test native histogram persistence

* Thu Feb 09 2023 xu_ping <xuping33@h-partners.com> - 1.4.1-5
- set flags for BIND_NOW

* Thu Feb 09 2023 xu_ping <xuping33@h-partners.com> - 1.4.1-4
- set flags for build

* Thu Jan 5 2023 caodongxia <caodongxia@h-partners.com> - 1.4.1-3
- Add buildRequires systemd

* Mon Oct 11 2021 chenchen <chen_aka_jan@163.com> - 1.4.1-2
- Logr shutdownr gracefully(#428

* Wed Jul 14 2021 baizhonggui <baizhonggui@huawei.com> - 1.4.1-1
- Package init

